import * as _ from "lodash";
import * as yargs from "yargs";
import * as scanner from "../scanner";

export var command = "sections";
export var describe = "Extracts a YAML field into sections";

export function builder(yargs: yargs.Arguments) {
    return yargs
        .help("help")

        .alias("f", "field")
        .default("field", "summary")

        .alias("t", "title")
        .default("title", "title")

        .alias("o", "output")
        .default("output", "-")

        .demand(1);
}

export function handler(argv: any) {
    var files = argv._.splice(1);
    var data = scanner.scanFiles(argv, files);
    render(argv, data);
}

export function render(argv, data) {
    for (var item of data) {
        var title = _.get(item, argv.title);
        var value = _.get(item, argv.field);

        console.log(`# ${title}`);
        console.log();

        if (value) {
            console.log(value.replace("\n", "\n\n"));
        }
    }
}
