import * as yargs from "yargs";
import * as scanner from "../scanner";
import * as table from "./table";

export var command = "count";
export var describe = "Counts the number of words in the given input";

export function builder(yargs: yargs.Arguments) {
    return yargs
        .help("help")

        .default("fields", ["_basename", "_words:r"])
        .describe(
            "fields",
            "The dotted fields in the metadata to display, or _basename for " +
                "the name of the file, or _words to count the number of words. " +
                "Appending a `:r` at the end right-aligns the output."
        )

        .default("table-start", "")
        .default("table-end", "")
        .default("table-delimiter", ": ")
        .boolean("table-rule")
        .default("table-rule", false)
        .boolean("table-header")
        .default("table-header", false)

        .boolean("total")
        .alias("t", "total")
        .describe(
            "total",
            "If provided, adds a special entry with a _basename of Totals " +
                "that adds up all the words."
        )

        .boolean("separator")
        .alias("s", "separator")
        .describe(
            "separator",
            "If provided, then the word counts will have comma separators " +
                "for thousands."
        )

        .alias("o", "output")
        .default("output", "-")

        .demand(1);
}

export function handler(argv: any) {
    var files = argv._.splice(1);
    var data = scanner.scanFiles(argv, files);

    if (argv.total) {
        argv.fields[1] += "t";
    }

    if (argv.separator) {
        argv.fields[1] += "s";
    }

    table.render(argv, data);
}
