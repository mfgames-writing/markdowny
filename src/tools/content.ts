import * as _ from "lodash";
import * as fs from "fs";
import * as scanner from "../scanner";
import * as yargs from "yargs";

export var command = "content";
export var describe = "Extracts the content of the file without metadata";

export function builder(yargs: yargs.Arguments) {
    return yargs
        .help("help")

        .option("output", {
            alias: "o",
            default: "-",
            describe: "Write output to a file or `-` for standard out",
        })

        .demand(1);
}

export function handler(argv: any) {
    // Parse through the files and retrieve the metadata.
    var files = argv._.splice(1);
    var data: any = scanner.scanFiles(argv, files);
    var output = data.map((x) => x.__content).join("\n");

    // Figure out where to write.
    if (argv.output === "-") {
        console.log(output);
    } else {
        fs.writeFileSync(argv.output, Buffer.from(output, "utf-8"));
    }
}
