import * as readPackage from "read-pkg-up";
import * as yargs from "yargs";

export var command = "version";
export var describe = "Displays version information about the program";

export function builder(yargs: yargs.Arguments) {
    return yargs.help("help").demand(0);
}

export function handler(argv: any) {
    const pkg: any = readPackage.sync();
    const json = pkg.pkg;

    console.log(`${json.name} v${json.version}`);
}
