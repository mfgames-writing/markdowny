## [0.2.1](https://gitlab.com/mfgames-writing/markdowny/compare/v0.2.0...v0.2.1) (2021-02-05)


### Bug Fixes

* correcting some documentation ([a5153e8](https://gitlab.com/mfgames-writing/markdowny/commit/a5153e8aa615bc9e39a3f1497614b238a73418b2))

## [0.0.3](https://gitlab.com/mfgames-writing/markdowny/compare/v0.0.2...v0.0.3) (2018-08-11)


### Bug Fixes

* added package management ([c7c8281](https://gitlab.com/mfgames-writing/markdowny/commit/c7c8281))
